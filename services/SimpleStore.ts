import { Getter, Mutation, State } from '~/node_modules/vuex-simple'

interface ProductDetail {
  name: string
  price: number
}

interface Product {
  id: string
  [key: string]: any
}

export class SimpleStore {
  @State()
  cartItems: Product[] = []
  @State()
  products: Map<string, ProductDetail> = new Map([
    [
      '10-SOCKS',
      {
        name: 'blue socks',
        price: 1.99
      }
    ],
    [
      '20-SOCKS',
      {
        name: 'red tube socks',
        price: 2.99
      }
    ],
    [
      '30-SOCKS',
      {
        name: 'ankle socks',
        price: 1.0
      }
    ],
    [
      '10-SHOES',
      {
        name: 'blue suede',
        price: 110.99
      }
    ],
    [
      '20-SHOES',
      {
        name: 'red converse all stars',
        price: 72.99
      }
    ],
    [
      '30-SHOES',
      {
        name: 'zip up ankle boot',
        price: 89.99
      }
    ]
  ])

  @State()
  filter: string = 'SHOES'

  @Mutation()
  showProduct(_value) {
    this.filter = _value
  }

  @Mutation()
  addItemToCart(_product: Product) {
    this.cartItems.push({
      id: _product.id,
      ...this.products.get(_product.id)
    })
  }

  @Mutation()
  removeItemFromCart(index) {
    this.cartItems.splice(index, 1)
    return this.cartItems
  }

  @Mutation()
  clearCart() {
    this.cartItems = []
  }

  @Getter()
  get cartSize() {
    return this.cartItems.length
  }

  @Getter()
  get currentFilter() {
    return this.filter
  }

  @Getter()
  get cartTotal() {
    return this.cartItems.reduce((total, item) => {
      return total + item.price
    }, 0.0)
  }

  @Getter()
  get filteredProducts() {
    console.log('filteredProducts: ' + this.filter)
    return this._filterByValue(this.products, this.filter)
  }

  @Getter()
  get _filterByValue() {
    return (products, filter) => {
      let result: Product[] = []
      products.forEach((v, k) => {
        if (k.indexOf(filter) !== -1 || filter === 'SHOW_ALL') {
          result.push({ ...v, id: k })
        }
      })
      return result
    }
  }
}
