import { SimpleStore } from '~/services/SimpleStore'
import { createVuexStore } from 'vuex-simple'

export default () => createVuexStore(new SimpleStore(), {})
